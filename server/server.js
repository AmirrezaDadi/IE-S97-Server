'use strict';
const express = require('express')
const app = express()
const houses = require('./data/houses.json')
/**
 * @api {get} /house Request general information of houses
 * @apiName GetAllHouses
 * @apiGroup PublicHouses
 *
 * @apiSuccess {String} result API request result
 * @apiSuccess {Array} houses list of houses
 * @apiSuccessExample {json} Success-Response: 
 {
 	"result": "ok",
 	"data": 
	[{
		"id": 1,
		"area": "80",
		"price": {
			"sellPrice": 3000
		},
		"dealType": 0,
		"imageURL": ""
	}, {
		"id": 4,
		"area": "1000",
		"price": {
			"basePrice": 3000000,
			"rentPrice": 100000
		},
		"dealType": 1,
		"imageURL": "https://upload.wikimedia.org/wikipedia/commons/thumb/b/b6/1122-WAS-The_White_House.JPG/270px-1122-WAS-The_White_House.JPG"
	}]
}
 * 
 */

app.get('/house', (req, res) => {
		let data = houses.map(house => {return {
			id: house.id,
			area: house.area,
			price: house.price,
			dealType: house.dealType,
			imageURL: house.imageURL

		}})
		res.status(200).json({
			result: "OK",
			data: data
		});
	}
);
/**
 * @api {get} /house/:id Request specific house information
 * @apiName GetHouseInfo
 * @apiGroup PublicHouses
 *
 * @apiSuccess {String} result API request result
 * @apiSuccess {json} house house information
 * @apiSuccessExample {json} Success-Response: 
 {
	"result": "OK",
	"data": {
		"id": 1,
		"area": "80",
		"buildingType": "آپارتمان",
		"address": "خ شمشیری",
		"imageURL": "",
		"dealType": 0,
		"price": {
			"sellPrice": 3000
		},
		"phone": "+989123456789",
		"description": "خیلی هم خوبه",
		"expireTime": "2018-10-10"
	}
}
 * 
 */
app.get('/house/:id', (req, res) => {
		let data = houses.find(house => house.id == req.params.id);
		res.status(200).json({
			result: "OK",
			data: data
		});
	}
);


app.listen(6663, () => console.log('Example app listening on port 3000!'))
